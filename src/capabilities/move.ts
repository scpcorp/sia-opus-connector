import iconsSvg from '../icons-svg'
import getMess from '../translations'
import sanitizeFilename from 'sanitize-filename'
const label = 'move'
import api from '../api'

async function handler({ dropRowResource, items }, actions) {
  const {
    showDialog,
    hideDialog,
    navigateToDir,
    updateNotifications,
    getSelectedResources,
    getResource,
    getNotifications
  } = actions

  await api.moveResources(dropRowResource, items)
  const resource = getResource()
  navigateToDir(resource.id, null, false, false)
}

export default (apiOptions, actions) => {
  const { getSelectedResources } = actions
  return {
    id: label,
    icon: { svg: iconsSvg.delete },
    label: 'Move',
    shouldBeAvailable: () => true,
    handler: (dropRowResource, items) => handler({ dropRowResource, items }, actions),
    availableInContexts: []
  }
}
